bolsonaro
==============================

Bolsonaro project of QARMA non-permanents: deforesting random forest using OMP.

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── notebooks          <- notebooks of prototypes etc
    │
    ├── models             <- trained and serialized models, model predictions, or model summaries
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so bolsonaro can be imported
    ├── bolsonaro          <- Source code for use in this project.
        ├── __init__.py    <- Makes bolsonaro a Python module
        │
        ├── data           <- Scripts to download or generate data (to store under `/data/*relevant directory*`)
        │   └── make_dataset.py
        │
        ├── models         <- Scripts to create base models (to store under `/models`)
        │   │                 
        │   └── create_model.py
        │
        └── visualization  <- Scripts to create exploratory and results oriented visualizations (to store under `/reports/figures`)
            └── visualize.py
     

--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

Instal project
--------------

First install the project pacakge:

	pip install -r requirements.txt

Then create a file `.env` by copying the file `.env.example`:
	
	cp .env.example .env
	
Then you must set the project directory in the `.env` file :
 
	project_dir = "path/to/your/project/directory"	

This directory will be used for storing the model parameters.