#!/bin/bash
core_number=5
walltime=$walltime
seeds='1 2 3 4 5'

for dataset in kin8nm kr-vs-kp spambase steel-plates california_housing boston iris diabetes digits wine breast_cancer olivetti_faces diamonds
do
    oarsub -p "(gpu is null)" -l /core=$core_number,walltime=$walltime "conda activate test_env && python code/train.py --dataset_name=$dataset --seeds $seeds --save_experiment_configuration 3 train-dev_subset --extracted_forest_size_stop=1 --extracted_forest_size_samples=30 --subsets_used=train,dev --experiment_id=1 --models_dir=models/$dataset/stage3"
    oarsub -p "(gpu is null)" -l /core=$core_number,walltime=$walltime "conda activate test_env && python code/train.py --dataset_name=$dataset --seeds $seeds --save_experiment_configuration 3 train-dev_train-dev_subset --extracted_forest_size_stop=1 --extracted_forest_size_samples=30 --subsets_used=train+dev,train+dev --experiment_id=2 --models_dir=models/$dataset/stage3"
    oarsub -p "(gpu is null)" -l /core=$core_number,walltime=$walltime "conda activate test_env && python code/train.py --dataset_name=$dataset --seeds $seeds --save_experiment_configuration 3 train-train-dev_subset --extracted_forest_size_stop=1 --extracted_forest_size_samples=30 --subsets_used=train,train+dev --experiment_id=3 --models_dir=models/$dataset/stage3"
done
