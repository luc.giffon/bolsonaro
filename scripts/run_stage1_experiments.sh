#!/bin/bash
core_number=5
walltime=1:00
seeds='1 2 3 4 5'

for dataset in kin8nm kr-vs-kp spambase steel-plates california_housing boston iris diabetes digits wine breast_cancer olivetti_faces diamonds
do
    oarsub -p "(gpu is null)" -l /core=$core_number,walltime=$walltime "conda activate test_env && python code/train.py --dataset_name=$dataset --seeds $seeds --save_experiment_configuration 1 none_with_params --extraction_strategy=none --extracted_forest_size_stop=1 --extracted_forest_size_samples=30 --experiment_id=1 --models_dir=models/$dataset/stage1"
    oarsub -p "(gpu is null)" -l /core=$core_number,walltime=$walltime "conda activate test_env && python code/train.py --dataset_name=$dataset --seeds $seeds --save_experiment_configuration 1 random_with_params --extraction_strategy=random --extracted_forest_size_stop=1 --extracted_forest_size_samples=30 --experiment_id=2 --models_dir=models/$dataset/stage1"
    oarsub -p "(gpu is null)" -l /core=$core_number,walltime=$walltime "conda activate test_env && python code/train.py --dataset_name=$dataset --seeds $seeds --save_experiment_configuration 1 omp_with_params --extracted_forest_size_stop=1 --extracted_forest_size_samples=30 --experiment_id=3 --models_dir=models/$dataset/stage1"
    oarsub -p "(gpu is null)" -l /core=$core_number,walltime=$walltime "conda activate test_env && python code/train.py --dataset_name=$dataset --seeds $seeds --save_experiment_configuration 1 none_wo_params --extraction_strategy=none --skip_best_hyperparams --extracted_forest_size_stop=1 --extracted_forest_size_samples=30 --experiment_id=4 --models_dir=models/$dataset/stage1"
    oarsub -p "(gpu is null)" -l /core=$core_number,walltime=$walltime "conda activate test_env && python code/train.py --dataset_name=$dataset --seeds $seeds --save_experiment_configuration 1 random_wo_params --extraction_strategy=random --skip_best_hyperparams --extracted_forest_size_stop=1 --extracted_forest_size_samples=30 --experiment_id=5 --models_dir=models/$dataset/stage1"
    oarsub -p "(gpu is null)" -l /core=$core_number,walltime=$walltime "conda activate test_env && python code/train.py --dataset_name=$dataset --seeds $seeds --save_experiment_configuration 1 omp_wo_params --skip_best_hyperparams --extracted_forest_size_stop=1 --extracted_forest_size_samples=30 --experiment_id=6 --models_dir=models/$dataset/stage1"
done
