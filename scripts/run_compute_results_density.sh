seeds='1 2 3 4 5'
for dataset in kin8nm kr-vs-kp spambase steel-plates california_housing boston iris diabetes digits wine breast_cancer olivetti_faces diamonds
do
    python code/compute_results.py --stage=1 --experiment_ids 1 2 3 4 5 6 --dataset_name=$dataset --models_dir=models/$dataset/stage1 --plot_weight_density --wo_loss_plots
    python code/compute_results.py --stage=2 --experiment_ids 1 2 3 4 --dataset_name=$dataset --models_dir=models/$dataset/stage2 --plot_weight_density --wo_loss_plots
    python code/compute_results.py --stage=3 --experiment_ids 1 2 3 --dataset_name=$dataset --models_dir=models/$dataset/stage3 --plot_weight_density --wo_loss_plots
    python code/compute_results.py --stage=4 --experiment_ids 1 2 3 --dataset_name=$dataset --models_dir=models/$dataset/stage4 --plot_weight_density --wo_loss_plots
done
