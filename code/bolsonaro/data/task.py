from enum import Enum


class Task(Enum):
    BINARYCLASSIFICATION = 1
    REGRESSION = 2
    MULTICLASSIFICATION = 3
