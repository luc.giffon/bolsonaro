class Dataset(object):

    def __init__(self, task, X_train, X_dev, X_test, y_train,
        y_dev, y_test):
        self._task = task
        self._X_train = X_train
        self._X_dev = X_dev
        self._X_test = X_test
        self._y_train = y_train
        self._y_dev = y_dev
        self._y_test = y_test

    @property
    def task(self):
        return self._task

    @property
    def X_train(self):
        return self._X_train
    
    @property
    def X_dev(self):
        return self._X_dev

    @property
    def X_test(self):
        return self._X_test

    @property
    def y_train(self):
        return self._y_train

    @property
    def y_dev(self):
        return self._y_dev

    @property
    def y_test(self):
        return self._y_test
