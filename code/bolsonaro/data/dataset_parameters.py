from bolsonaro.utils import save_obj_to_json, load_obj_from_json

import os


class DatasetParameters(object):

    def __init__(self, name, test_size, dev_size, random_state, dataset_normalizer):
        self._name = name
        self._test_size = test_size
        self._dev_size = dev_size
        self._random_state = random_state
        self._dataset_normalizer = dataset_normalizer

    @property
    def name(self):
        return self._name

    @property
    def test_size(self):
        return self._test_size

    @property
    def dev_size(self):
        return self._dev_size

    @property
    def random_state(self):
        return self._random_state

    @property
    def dataset_normalizer(self):
        return self._dataset_normalizer

    def save(self, directory_path, experiment_id):
        save_obj_to_json(directory_path + os.sep + 'dataset_parameters_{}.json'.format(experiment_id),
            self.__dict__)

    @staticmethod
    def load(directory_path, experiment_id):
        return load_obj_from_json(directory_path + os.sep + 'dataset_parameters_{}.json'.format(experiment_id),
            DatasetParameters)
